#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  makeSSUdb: creating SSU databank from NCBI to be used by ASSU                                             #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2024 Institut Pasteur"                                                           #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr  #
#   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)             research.pasteur.fr/en/b/VTq  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=1.1;                                                                                               #
# + best compression ratio                                                                                   #
#                                                                                                            #
# VERSION=1.0;                                                                                               #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# -- gzip -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  GZIP_BIN=gzip;
  if [ ! $(command -v $GZIP_BIN) ]; then echo "no $GZIP_BIN detected" >&2 ; exit 1 ; fi
  GZIP_STATIC_OPTIONS="--quiet";     
  GZIP="$GZIP_BIN $GZIP_STATIC_OPTIONS";
  GUNZIP="$GZIP --decompress";
#                                                                                                            #
# -- wget | curl ------------------------------------------------------------------------------------------  #
#                                                                                                            #
  WGET_BIN=wget;
  CURL_BIN=curl;
  USE_WGET=true;
  if [ $(command -v $WGET_BIN) ]
  then
    WGET_STATIC_OPTIONS="--quiet --retry-connrefused --no-check-certificate";     
    WGET="$WGET_BIN $WGET_STATIC_OPTIONS";
  elif [ $(command -v $CURL_BIN) ]
  then
    USE_WGET=false;
    CURL_STATIC_OPTIONS="--silent --location --continue-at -";     
    CURL="$CURL_BIN $CURL_STATIC_OPTIONS";
  else
    echo "neither $WGET_BIN nor $CURL_BIN detected" >&2 ;
    exit 1 ;
  fi
#                                                                                                            #
##############################################################################################################

echo "makeSSUdb v$VERSION $COPYRIGHT" ;
  
##############################################################################################################
#                                                                                                            #
# CREATING TMP DIR                                                                                           #
#                                                                                                            #
##############################################################################################################
TMP_DIR=$(mktemp -d -p $(pwd) XXXXXXXXXX);
finalize() { rm -r $TMP_DIR ; }
trap 'finalize;exit 1' SIGTERM SIGINT SIGQUIT SIGHUP TERM INT QUIT HUP ;

##############################################################################################################
#                                                                                                            #
# DOWNLOADING SEQUENCE FILES                                                                                 #
#                                                                                                            #
##############################################################################################################
FTPURL="https://ftp.ncbi.nlm.nih.gov/refseq/TargetedLoci";
echo -n "downloading ." ;
if $USE_WGET
then while true ; do echo -n "." ; $WGET -O $TMP_DIR/arc.fna.gz  $FTPURL/Archaea/archaea.16SrRNA.fna.gz   && break ; done
else while true ; do echo -n "." ; $CURL -o $TMP_DIR/arc.fna.gz  $FTPURL/Archaea/archaea.16SrRNA.fna.gz   && break ; done
fi
echo -n "." ; 
if $USE_WGET
then while true ; do echo -n "." ; $WGET -O $TMP_DIR/bac.fna.gz  $FTPURL/Bacteria/bacteria.16SrRNA.fna.gz   && break ; done
else while true ; do echo -n "." ; $CURL -o $TMP_DIR/bac.fna.gz  $FTPURL/Bacteria/bacteria.16SrRNA.fna.gz   && break ; done
fi
echo ". [ok]" ;

##############################################################################################################
#                                                                                                            #
# READING SEQUENCE FILES                                                                                     #
#                                                                                                            #
##############################################################################################################
echo -n "reading .." ;
$GUNZIP -c $TMP_DIR/arc.fna.gz |
  gawk '!/^>/  {s=s$0;next}
        (s!=""){print s;s=""}
               {print}                
        END    {print s}' | #          | starts with A == archaea
    paste - - | tr '\t' ' ' | sed 's/>/A /g;s/16S ribosomal RNA, //g;s/ sequence//g' | tr -d []\' | sort -k3 > $TMP_DIR/arc.txt ;
echo -n ".." ;
rm -f $TMP_DIR/arc.fna.gz ;
echo -n ".." ;
$GUNZIP -c $TMP_DIR/bac.fna.gz |
  gawk '!/^>/  {s=s$0;next}
        (s!=""){print s;s=""}
               {print}
        END    {print s}' | #          | starts with B == bacteria
    paste - - | tr '\t' ' ' | sed 's/>/B /g;s/16S ribosomal RNA, //g;s/ sequence//g' | tr -d []\' | sort -k3 > $TMP_DIR/bac.txt ;
echo -n ".." ;
rm -f $TMP_DIR/bac.fna.gz ;
echo ". [ok]" ;

##############################################################################################################
#                                                                                                            #
# PROCESSING SEQUENCE FILES                                                                                  #
#                                                                                                            #
##############################################################################################################
echo -n "processing ..." ;
for f in $TMP_DIR/arc.txt $TMP_DIR/bac.txt
do
  gawk 'function c(s)  {return length(s)-gsub("N","N",s)}
        (NR==1)        {best=$0;gnsp=$3":"$4;max=c($NF);next}
        ($3":"$4!=gnsp){print best;
                        best=$0;gnsp=$3":"$4;max=c($NF);next}
        (max<c($NF))   {best=$0;       max=c($NF)}
        END            {print best}' $f ; 
done | gawk '{printf">"$2" "$1;  
              i=2;while(++i < NF)printf" "$i;print""; print$NF}' > $TMP_DIR/ssu.db ;
echo "... [ok]" ;

##############################################################################################################
#                                                                                                            #
# FINALIZING                                                                                                 #
#                                                                                                            #
##############################################################################################################
echo -n "compressing .." ;
n=$(grep -c "^>" $TMP_DIR/ssu.db);
echo -n "." ;
$GZIP --best -c $TMP_DIR/ssu.db > SSUdb.gz ;
echo -n "." ;
{ echo  -n "SSUdb.gz  " ;
  date +"%Y-%m-%d %H:%M:%S" ;
  echo "$n sequences" ;
  echo "built using makeSSUdb v$VERSION" ; } > SSUdb.version.txt ;
finalize ;
echo ". [ok]" ;

echo ">> SSU databank:  SSUdb.gz" ;
echo ">> SSUdb version: SSUdb.version.txt" ;
echo ">> no. sequences: $n" ;

echo "exit" ;

exit ;
