#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  ASSU: ASSembling SSU                                                                                      #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2024 Institut Pasteur"                                                           #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr  #
#   Centre de Ressources Biologiques de l'Institut Pasteur (CRBIP)             research.pasteur.fr/en/b/VTq  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=1.1;                                                                                               #
# + SSUdb inside a dedicated directory ($SSUDB_DIR)                                                          #
# + modified model sequence selection criterion                                                              #
# + updated verbose (option -v)                                                                              #
#                                                                                                            #
# VERSION=1.0;                                                                                               #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# =============                                                                                              #
# = CONSTANTS =                                                                                              #
# =============                                                                                              #
#                                                                                                            #
# -- PWD: directory containing the current script ---------------------------------------------------------  #
#                                                                                                            #
  PWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)";
#                                                                                                            # 
# -- extensions -------------------------------------------------------------------------------------------  #
#                                                                                                            #
  declare -a FILE_EXTENSIONS=("fq" "fastq" "gz" "bz" "bz2" "dsrc" "dsrc2");
#                                                                                                            #
# -- n/a --------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  NA="._N.o.N._.A.p.P.l.I.c.A.b.L.e_.";
#                                                                                                            #
# -- headers ----------------------------------------------------------------------------------------------  #
#                                                                                                            #
  H1="10        20        30        40        50        60        70        80        90       100";
  H2=" |         |         |         |         |         |         |         |         |         |";
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# - SUMMARY -                                                                                                #
# bwa_mem2/2.2.1 bzip2/1.0.6 dsrc/2.0.2 gawk/5.0.1 samtools/1.18 
#                                                                                                            #
# -- gawk -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  GAWK_BIN=gawk;
#                                                                                                            #
# -- bzip2 ------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  BZIP2_BIN=bzip2;
#                                                                                                            #
# -- gunzip -----------------------------------------------------------------------------------------------  #
#                                                                                                            #
  GUNZIP_BIN=gunzip;
#                                                                                                            #
# -- zgrep ------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  ZGREP_BIN=zgrep;
#                                                                                                            #
# -- pigz -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  PIGZ_BIN=pigz;
#                                                                                                            #
# -- DSRC -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  DSRC_BIN=dsrc;
#                                                                                                            #
# -- bwa-mem2 ---------------------------------------------------------------------------------------------  #
#                                                                                                            #
  BWAMEM2_BIN=bwa-mem2;
#                                                                                                            #
# -- samtools ---------------------------------------------------------------------------------------------  #
#                                                                                                            #
  SAMTOOLS_BIN=samtools;
#                                                                                                            #
##############################################################################################################
  
##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = FITTINGS     =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# -- gawk -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  BAWK="$GAWK_BIN";
  CAWK="$GAWK_BIN -F,";
  TAWK="$GAWK_BIN -F\\t";
#                                                                                                            #
# -- bzip -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  BUNZIP="$BZIP2_BIN --decompress";
#                                                                                                            #
# -- DSRC -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  DUNZIP="$DSRC_BIN d";
#                                                                                                            #
# -- gunzip -----------------------------------------------------------------------------------------------  #
#                                                                                                            #
  GUNZIP="$GUNZIP_BIN";
#                                                                                                            #
# -- pigz -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  PUNZIP="$PIGZ_BIN --decompress";
#                                                                                                            #
# -- zgrep ------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  ZGREP="$ZGREP_BIN";
#                                                                                                            #
# -- bwa-mem2 ---------------------------------------------------------------------------------------------  #
#                                                                                                            #
  BWA_INDEX="$BWAMEM2_BIN index";
  BWA_MEM="$BWAMEM2_BIN mem -v 0 -K 100000000 -Y -L 20";
#                                                                                                            #
# -- samtools: exploring read alignments ------------------------------------------------------------------  #
#                                                                                                            #
  SAMTOOLS_SORT="$SAMTOOLS_BIN sort --output-fmt BAM -l 1 -m 1073741824";
  SAMTOOLS_CONSENSUS="$SAMTOOLS_BIN consensus --format FASTA --show-del no --show-ins yes"; 
#                                                                                                            #
##############################################################################################################
  
##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = DOC      =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
mandoc() {
  echo -e "\n\033[1m ASSU v$VERSION                                           $COPYRIGHT\033[0m";
  cat <<EOF

 https://gitlab.pasteur.fr/GIPhy/ASSU

 USAGE:  ASSU  [options]  <infile> [<infile> ...]

 OPTIONS:
  -d <file>    SSU databank file (default: db/SSUdb.gz in the same directory as ASSU)
  -p <string>  restricts the  SSU databank  to the  specified  (extended regex)  pattern
               (default: none)
  -o <string>  output FASTA-formatted SSU sequence file name (default: ssu.fasta)
  -O <string>  writes the selected  reads into the  specified FASTQ-formatted  file name
               (default: none)
  -l <int>     minimum sequence length (default: 1000)
  -L <int>     minimum read length (default: AUTO)
  -Q <int>     minimum base Phred quality value (default: 20)
  -M <int>     minimum mapping Phred quality value (default: 20)
  -D <int>     minimum coverage depth (default: 50)
  -F <float>   minimum proportion of the majority base to infer that base (default: 0.8)
  -A <float>   minimum ratio of the alternative  base(s) to the majority one to add that
               base(s) to the consensus (default: 0.2)
  -N           set N when multiple bases at a consensus position (default: not set)
  -w <dir>     path to the tmp directory (default: \$TMPDIR, otherwise /tmp)
  -t <int>     thread numbers (default: 2)
  -v           verbose mode
  -s           prints the content of the SSU databank and exit
  -c           checks dependencies and exit
  -h           prints this help and exit

 EXAMPLES:
  ASSU  -t 24  -o 16s.fasta  fwd.fastq.gz  rev.fastq.gz  sgl.fastq.gz
  ASSU  -d SSUdb.gz  -O 16s.fastq  -p "Devosia limi"  -L 75  -v  *.fastq
  ASSU  -p "Citrobacter|Escherichia|Shigella"  -N  -v  hts.fastq.bz2

EOF
} 
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# =============                                                                                              #
# = FUNCTIONS =                                                                                              #
# =============                                                                                              #
#                                                                                                            #
# -- echoxit() --------------------------------------------------------------------------------------------- #
# >> prints in stderr the specified error message $1 and next exit 1                                         #
#                                                                                                            #
echoxit() {
  echo "$1" >&2 ; exit 1 ;
}    
#                                                                                                            #
# -- chrono -----------------------------------------------------------------------------------------------  #
# >> returns the elapsed time in well-formatted format                                                       #
#                                                                                                            #
chrono() {
  local s=$SECONDS; printf "[%02d:%02d]" $(( $s / 60 )) $(( $s % 60 )) ;
}
#                                                                                                            #
# -- dcontrol ---------------------------------------------------------------------------------------------  #
# >> controls required and optional (non-coreutils) dependancies                                             #
#                                                                                                            #
dcontrol() {
  fail=false;
  for binexe in  bc  grep  $ZGREP_BIN  sed  $GAWK_BIN  $BWAMEM2_BIN  $SAMTOOLS_BIN
  do
    if [ ! $(command -v $binexe) ]; then echo "[ERROR] $binexe not found" >&2 ; fail=true; fi
  done
  if [ ! $(command -v $GUNZIP_BIN) ] && [ ! $(command -v $PIGZ_BIN) ]
  then
    echo "[ERROR] $GUNZIP_BIN or $PIGZ_BIN not found" >&2 ;
    fail=true;
  fi
  for binexe in  $BZIP2_BIN $DSRC_BIN
  do
    if [ ! $(command -v $binexe) ]; then echo "[WARNING] $binexe not found" ; fi
  done
  if $fail ; then exit 1 ; fi
}
#                                                                                                            #
# -- dcheck -----------------------------------------------------------------------------------------------  #
# >> checks (non-coreutils) dependancies and exit                                                            #
#                                                                                                            #
dcheck() {
  echo "Checking required dependencies ..." ;
  ## bc ##############################
  echo -e -n "> \e[1mbc\e[0m          >1.0       mandatory\t" ;       binexe=bc;
  echo -e -n "$binexe      \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   bc -v | head -1 ;
  fi
  ## grep ############################
  echo -e -n "> \e[1mgrep\e[0m        >1.0       mandatory\t" ;       binexe=grep;
  echo -e -n "$binexe    \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   grep -V | head -1 ;
  fi
  ## zgrep ###########################
  echo -e -n "> \e[1zzgrep\e[0m       >1.0       mandatory\t" ;       binexe=$ZGREP_BIN;
  echo -e -n "$binexe    \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $ZGREP_BIN -V | head -1 ;
  fi
  ## sed #############################
  echo -e -n "> \e[1msed\e[0m         >1.0       mandatory\t" ;       binexe=sed;
  echo -e -n "$binexe     \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   sed --version | head -1 ;
  fi
  ## gawk >=4.0.0 ####################
  echo -e -n "> \e[1mgawk\e[0m        >4.0.0     mandatory\t" ;       binexe=$GAWK_BIN;
  echo -e -n "$binexe    \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $GAWK_BIN -V | head -1 ;
  fi
  ## bzip2 ###########################
  echo -e -n "> \e[1mbzip2\e[0m       >1.0       optional\t" ;        binexe=$BZIP2_BIN;
  echo -e -n "$binexe    \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $BZIP2_BIN --help 2>&1 | head -1 ;
  fi
  ## DSRC ############################
  echo -e -n "> \e[1mDSRC\e[0m        >=2.0      optional\t" ;        binexe=$DSRC_BIN;
  echo -e -n "$binexe    \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $DSRC_BIN 2>&1 | sed -n 2p ;
  fi
  ## gunzip ##########################
  echo -e -n "> \e[1mgunzip\e[0m      >1.0       mandatory\t" ;       binexe=$GUNZIP_BIN;
  echo -e -n "$binexe    \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $GUNZIP_BIN -V | head -1 ;
  fi
  ## pigz ############################
  echo -e -n "> \e[1mpigz\e[0m        >=2.4      optional\t" ;        binexe=$PIGZ_BIN;
  echo -e -n "$binexe    \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $PIGZ_BIN -V ;
  fi
  ## bwa-mem2 >=2.2.1 ################
  echo -e -n "> \e[1mBWA-mem2\e[0m    >=2.2.1    mandatory\t" ;       binexe=$BWAMEM2_BIN;
  echo -e -n "$binexe\t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $BWAMEM2_BIN version ;
  fi
  ## samtools >= 1.18 ################
  echo -e -n "> \e[1mSamtools\e[0m    >=1.18     mandatory\t" ;       binexe=$SAMTOOLS_BIN;
  echo -e -n "$binexe  \t\t" ;
  if [ ! $(command -v $binexe) ]; then echo -e "\e[31m[fail]\e[0m";
  else        echo -e -n "$(which $binexe)\t\t\e[32m[ok]\e[0m\t\t";   $SAMTOOLS_BIN --version | head -1;
  fi
  echo "[exit]" ;
  exit ;
}
# -- dispdb -----------------------------------------------------------------------------------------------  #
# >> displays the content of the specified SSU databank                                                      #
#                                                                                                            #
dispdb() {
   $GUNZIP -c $1 2>/dev/null |
     tr -d '>' | paste - - | tr '\t' ' ' |
       $BAWK '   {if($2=="A"){k="Archaea";++a}
                  else       {k="Bacteria";++b}
                  if($3=="Candidatus")print k" | "$1" | "length($NF)" bps | "$3" "$4" "$5;
                  else                print k" | "$1" | "length($NF)" bps | "$3" "$4}
              END{print"# Archaea:  "a;
                  print"# Bacteria: "b}' ;
}
#                                                                                                            #
##############################################################################################################


##############################################################################################################
#                                                                                                            #
# READING OPTIONS                                                                                            #
#                                                                                                            #
##############################################################################################################

if [ $# -lt 1 ]; then mandoc ; exit 1 ; fi

SSUDB_DIR=${SSUDB_DIR:-$PWD/db}
SSUDB=$SSUDB_DIR/SSUdb.gz; # SSU databank   -d
OUTFILE=ssu.fasta;         # FASTA outfile  -o
OUTFQ="$NA";               # FASTQ outfile  -O
PATTERN="$NA";             # pattern        -p
SLGT=1000;                 # min seq lgt    -l
RLGT="00";                 # min read lgt   -L
BQ=20;                     # min Phred      -Q
MQ=20;                     # min map Phread -M
COV=50;                    # min cov depth  -D
FQ=0.8;                    # min freq       -F
FH=0.2;                    # min het        -A
AMB=true;                  # only N         -N 
DISPDB=false;              # display SSUdb  -s        
TMP_DIR=${TMPDIR:-/tmp};   # tmp directory  -w
NTHREADS=2;                # no. threads    -t
VERBOSE=false;             # verbose mode   -v        
DEBUG=false;               # debug mode     -X

while getopts d:p:o:O:l:L:Q:M:D:F:A:w:t:NvcshX option
do
  case $option in
  d)  SSUDB="$OPTARG"    ;;
  p)  PATTERN="$OPTARG"  ;;
  o)  OUTFILE="$OPTARG"  ;;
  O)  OUTFQ="$OPTARG"    ;;
  l)  SLGT=$OPTARG       ;;
  L)  RLGT=$OPTARG       ;;
  Q)  BQ=$OPTARG         ;;
  M)  MQ=$OPTARG         ;;
  D)  COV=$OPTARG        ;;
  F)  FQ="$OPTARG"       ;;
  A)  FH="$OPTARG"       ;;
  N)  AMB=false          ;;
  w)  TMP_DIR="$OPTARG"  ;;
  t)  NTHREADS=$OPTARG   ;;
  v)  VERBOSE=true       ;;
  c)  dcheck             ;;
  s)  DISPDB=true        ;;
  X)  DEBUG=true         ;;
  h)  mandoc ;  exit 0   ;;
  \?) mandoc ;  exit 1   ;;
  esac
done

shift "$(( $OPTIND - 1 ))"
NFILE=$#;      # no. input file(s)
FQLIST="$@";   # specified input file(s)

$DEBUG && VERBOSE=true;


echo "# ASSU v$VERSION" ;
echo "# $COPYRIGHT" ;
echo "+ https://gitlab.pasteur.fr/GIPhy/ASSU" ;
if $VERBOSE
then
  echo "> Syst: $MACHTYPE" ;
  echo "> Bash: $BASH_VERSION" ;
  echo "> SSUdb: $SSUDB" ;
  if [ -s ${SSUDB%.*}.version.txt ]
  then
    $BAWK '(NR==1){d=$2}
           (NR==2){n=$1}
           END    {print"> SSUdb v"d" ("n" sequences)"}' ${SSUDB%.*}.version.txt
  fi
fi

[ ! -e $SSUDB ]                     && echoxit "[ERROR] databank not found (option -d): $SSUDB" ;
[   -d $SSUDB ]                     && echoxit "[ERROR] not a file (option -d): $SSUDB" ;
[ ! -s $SSUDB ]                     && echoxit "[ERROR] empty file (option -d): $SSUDB" ;
[ ! -r $SSUDB ]                     && echoxit "[ERROR] no read permission (option -d): $SSUDB" ;
if $DISPDB
then
  dispdb $SSUDB ;
  exit ;
fi
[[ $SLGT =~ ^[0-9]+$ ]]             || echoxit "[ERROR] incorrect value (option -l): $SLGT" ; 
[[ $RLGT =~ ^[0-9]+$ ]]             || echoxit "[ERROR] incorrect value (option -L): $RLGT" ; 
[[ $BQ =~ ^[0-9]+$ ]]               || echoxit "[ERROR] incorrect value (option -Q): $BQ" ; 
[[ $MQ =~ ^[0-9]+$ ]]               || echoxit "[ERROR] incorrect value (option -M): $MQ" ; 
[[ $COV =~ ^[0-9]+$ ]]              || echoxit "[ERROR] incorrect value (option -D): $COV" ; 
[[ $FQ =~ ^[0-9]+([.][0-9]+)?$ ]]   || echoxit "[ERROR] incorrect value (option -F): $FQ" ; 
[ $(bc <<<"0<=$FQ&&$FQ<=1") -eq 0 ] && echoxit "[ERROR] incorrect value (option -F): $FQ" ; 
[[ $FH =~ ^[0-9]+([.][0-9]+)?$ ]]   || echoxit "[ERROR] incorrect value (option -H): $FH" ; 
[ $(bc <<<"0<=$FH&&$FH<=1") -eq 0 ] && echoxit "[ERROR] incorrect value (option -H): $FH" ; 
[[ $NTHREADS =~ ^[0-9]+$ ]]         || echoxit "[ERROR] incorrect value (option -t): $NTHREADS" ; 
 [ $NTHREADS -lt 1 ] && NTHREADS=1;


##############################################################################################################
#                                                                                                            #
# CHECKING DEPENDANCIES AND AVAILABLE ZIPPERS                                                                #
#                                                                                                            #
##############################################################################################################
BZ=false; [ -s "$(command -v $BZIP2_BIN)" ]  && BZ=true;
DZ=false; [ -s "$(command -v $DSRC_BIN)" ]   && DZ=true;
GZ=false; [ -s "$(command -v $GUNZIP_BIN)" ] && GZ=true;
PZ=false; [ -s "$(command -v $PIGZ_BIN)" ]   && PZ=true;

dcontrol;


##############################################################################################################
#                                                                                                            #
# CHECKING INFILES                                                                                           #
#                                                                                                            #
##############################################################################################################
if   [ $NFILE -eq 0 ]; then echoxit "[ERROR] no input file" ;
elif [ $NFILE -eq 1 ]; then MESSAGE="checking input file" ;
else                        MESSAGE="checking input files" ;
fi
echo -n "$(chrono) $MESSAGE ... " ;

ouch=false;
for f in $FQLIST
do
  if [ ! -e $f ];        then ! $ouch && echo ; echo "[ERROR] file not found: $f"           >&2 ; ouch=true; continue; fi
  if [   -d $f ];        then ! $ouch && echo ; echo "[ERROR] not a file: $f"               >&2 ; ouch=true; continue; fi
  if [ ! -s $f ];        then ! $ouch && echo ; echo "[ERROR] empty file: $f"               >&2 ; ouch=true; continue; fi
  if [ ! -r $f ];        then ! $ouch && echo ; echo "[ERROR] no read permission: $f"       >&2 ; ouch=true; continue; fi
  fext="${f##*.}";
  valid=false;
  for e in "${FILE_EXTENSIONS[@]}"
  do
    if [ "$e" == "$fext" ]; then valid=true; break; fi
  done
  if ! $valid ;          then ! $ouch && echo ; echo "[ERROR] file extension not valid: $f" >&2 ; ouch=true; continue; fi
  case $fext in
  bz|bz2)     if ! $BZ ; then ! $ouch && echo ; echo "[ERROR] bzip2 not available: $f"      >&2 ; ouch=true; continue; fi ;;
  dsrc|dsrc2) if ! $DZ ; then ! $ouch && echo ; echo "[ERROR] dsrc not available: $f"       >&2 ; ouch=true; continue; fi ;;
  esac
done
$ouch && exit 1 ;

echo "[ok]" ;

if $VERBOSE ; then for f in $FQLIST ; do echo "+ $f" ; done ; fi


##############################################################################################################
#                                                                                                            #
# CREATING TMP DIRECTORY, DEFINING TRAP AND TMP FILES, ESTIMATING RLGT                                       #
#                                                                                                            #
##############################################################################################################
MESSAGE="creating tmp directory" ;
echo -n "$(chrono) $MESSAGE ." ;

[ "${TMP_DIR:0:1}" != "/" ] && TMP_DIR="$(pwd)/$TMP_DIR";
if [ ! -e $TMP_DIR ]; then echo ; echo "[ERROR] tmp directory does not exist (option -w): $TMP_DIR" >&2 ; exit 1 ; fi
if [ ! -d $TMP_DIR ]; then echo ; echo "[ERROR] not a directory (option -w): $TMP_DIR"              >&2 ; exit 1 ; fi
if [ ! -w $TMP_DIR ]; then echo ; echo "[ERROR] no write permission (option -w): $TMP_DIR"          >&2 ; exit 1 ; fi

echo -n "." ;

TMP_DIR=$(mktemp -d -p $TMP_DIR ASSU.XXXXXXXXXX);
finalize() { rm -r $TMP_DIR ; }
trap 'finalize ; exit 1' SIGTERM SIGINT SIGQUIT SIGHUP TERM INT QUIT HUP ;

SSU_IDX=$TMP_DIR/ssu;
BAM=$TMP_DIR/aln.bam;
SAM=$TMP_DIR/aln.sam;
FQTMP=$TMP_DIR/htsr.fastq;
REF_SEQ=$TMP_DIR/ref.fa;  
REF_IDX=$TMP_DIR/ref;
TMP1=$TMP_DIR/tmp1.txt;
TMP2=$TMP_DIR/tmp2.txt;

echo -n "." ;

if [ "$RLGT" == "00" ]
then
  [ $NTHREADS -gt 24 ] && DSRCopt="-t24 -s" || DSRCopt="-t$NTHREADS -s" ;
  for f in $FQLIST
  do
    fext="${f##*.}";
    case $fext in
    fq|fastq)                sed -n 2~4p             $f                           | $BAWK '(FNR>10000){exit}{print length()}'      ;;
    gz)        if $PZ ; then $PUNZIP -c -p $NTHREADS $f 2>/dev/null | sed -n 2~4p | $BAWK '(FNR>10000){exit}{print length()}' ;
	                      else $GUNZIP -c              $f 2>/dev/null | sed -n 2~4p | $BAWK '(FNR>10000){exit}{print length()}' ; fi ;;
    bz|bz2)                  $BUNZIP -c              $f 2>/dev/null | sed -n 2~4p | $BAWK '(FNR>10000){exit}{print length()}'      ;;
    dsrc|dsrc2)              $DUNZIP    $DSRCopt     $f 2>/dev/null | sed -n 2~4p | $BAWK '(FNR>10000){exit}{print length()}'      ;;
    esac
  done > $TMP1 ; 

  echo -n "." ; 
  
  sort -rn $TMP1 > $TMP2 ;
  n=$(cat $TMP2 | wc -l)
  n=$(( $n / 10 ));

  if [ $n -eq 0 ]
  then
    echo "" ;
    echo "> empty file(s)" ;
    finalize ;
    echo "$(chrono) exit" ;
    exit ;
  fi
  
  RLGT=$(sed -n "$n"p $TMP2);
  RLGT=$(( 90 * $RLGT / 100 )); # 90% of the 90th centile of the read lengths
fi

echo " [ok]" ;

if $VERBOSE ; then echo "> TMP_DIR=$TMP_DIR" ; fi


##############################################################################################################
#                                                                                                            #
# SEARCHING SSU DATABANK                                                                                     #
#                                                                                                            #
##############################################################################################################
MESSAGE="examining SSU databank" ;
echo -n "$(chrono) $MESSAGE ." ;

## indexing SSU databank #######################################################
if [ "$PATTERN" != "$NA" ] && [ $($ZGREP -c -E "$PATTERN" $SSUDB) -eq 0 ]
then
  echo ".. [ok]" ;
  echo "> no selected sequence with the specified pattern: $PATTERN" ;
  finalize ;
  exit ;
fi
$BWA_INDEX -p $SSU_IDX $SSUDB &>/dev/null ;

echo -n "." ;

## aligning HTS reads ##########################################################
touch $SAM ;
[ $NTHREADS -gt 24 ] && DSRCopt="-t24" || DSRCopt="-t$NTHREADS" ;
BWAopt="-t $NTHREADS -k 21 -w 50";
for fin in $FQLIST
do
  echo -n "." ;
  fext="${fin##*.}";
  case $fext in
  fq|fastq|gz)                                              f=$fin   ;;
  bz|bz2)      $BUNZIP       -c $fin > $FQTMP 2>/dev/null ; f=$FQTMP ;;
  dsrc|dsrc2)  $DUNZIP $DSRCopt $fin   $FQTMP 2>/dev/null ; f=$FQTMP ;;
  esac
  echo -n "." ;
  $BWA_MEM $BWAopt $SSU_IDX $f 2>/dev/null | $TAWK '/^@/||($2==4){next}{print}' >> $SAM ;
done

echo " [ok]" ;

## getting ref #################################################################
if $DEBUG
then
  echo -e "no.hits\t\tsum.scores\tavg.score\tinfo" ;
  $TAWK '{print$3"\t"$14}' $SAM |                                
    sed 's/AS:i://g' |
      $TAWK '   {m[$1]+=$2;++n[$1]}
             END{for(a in m)if(n[a]>10)print n[a]"\t"m[a]"\t"(m[a]/n[a])"\t"a}' | 
        sort -rn | head |
          while read x y z a ; do echo -e "$x\t\t$y\t\t$z\t\t$($ZGREP -m1 -F $a $SSUDB)" ; done | tr -d '>' ;
fi

if [ "$PATTERN" == "$NA" ]
then
  # maximizing the sum of alignment scores (field 14; AS:i:)
  accn=$($TAWK '{print$3"\t"$14}' $SAM | sed 's/AS:i://g' | 
           $TAWK '{s[$1]+=$2}END{for(a in s)print s[a]"\t"a}' | 
             sort -rg | $TAWK '(NR==1){print$2;exit}');
else
  # selecting accession(s) using $PATTERN
  $ZGREP -E "$PATTERN" $SSUDB | $BAWK '{print$1}' | tr -d '>' > $TMP1 ; 
  # maximizing the sum of alignment scores (AS:i:) among the selected reference sequences
  accn=$($TAWK '{print$3"\t"$14}' $SAM | sed 's/AS:i://g' |
           $TAWK '{s[$1]+=$2}END{for(a in s)print s[a]"\t"a}' |
             grep -F -f $TMP1 |
               sort -rg | $TAWK '(NR==1){print$2;exit}');
fi

if [ -z "$accn" ]
then
  $VERBOSE && [ "$PATTERN" != "$NA" ] && echo "> selection pattern: $PATTERN" ;
  echo "> no SSU found" ;
  finalize ;
  echo "$(chrono) exit" ;
  exit ;
fi

$ZGREP -m1 -A1 -F ">$accn" $SSUDB > $REF_SEQ ;

line="$(head -1 $REF_SEQ)";
case $($BAWK '{print$2}' <<<"$line") in
A) kingdom="Archaea" ;;
B) kingdom="Bacteria" ;;
esac
genus=$($BAWK '{print$3}' <<<"$line");
if [ "$genus" == "Candidatus" ]
then taxo="$($BAWK '{print$3" "$4" "$5}' <<<"$line")";
else taxo="$($BAWK '{print$3" "$4}' <<<"$line")";
fi
line="$(tail -1 $REF_SEQ)";
LREF=${#line};

$VERBOSE && [ "$PATTERN" != "$NA" ] && echo "> selection pattern: $PATTERN" ;
$VERBOSE && echo "> model:  $kingdom  |  $taxo  |  $accn  |  $LREF bps" ;


##############################################################################################################
#                                                                                                            #
# BUILDING SSU SEQUENCE                                                                                      #
#                                                                                                            #
##############################################################################################################
MESSAGE="building SSU sequence" ;
echo -n "$(chrono) $MESSAGE ." ;

## getting HTS reads ###########################################################
$TAWK -v l=$RLGT '(length($10)>=l){print$10"\t"$11}' $SAM |  
  sort -u | $TAWK '{print"@SSU:"NR;
                    print$1;
                    print"+";
                    print$2}' > $FQTMP ;

echo -n "." ;

line=$(sed -n 2~4p $FQTMP | $TAWK '{b+=length()}END{print NR" "b}');
NR=$($BAWK '{print$1}' <<<"$line"); # no. reads
NB=$($BAWK '{print$2}' <<<"$line"); # no. bases
DEPTH=$(( $NB / $LREF ));

if [ $DEPTH -lt $COV ]
then
  echo ". [ok]" ; 
  echo "> low coverage depth: $DEPTH""""x" ;
  finalize ;
  echo "$(chrono) exit" ;
  exit ;
fi

## indexing reference sequence #################################################
$BWA_INDEX -p $REF_IDX $REF_SEQ &>/dev/null ;

echo -n "." ;

## mapping HTS reads ###########################################################
t=$(( 2 * $NTHREADS / 3 )); 
[ $t -lt 1 ] && t=1;
BWAopt="-t $t -D 0.1 -r 0.1 -k 7";
t=$(( $NTHREADS - $t )); 
[ $t -lt 1 ] && t=1;
STSopt="--threads $t -T $TMP_DIR" ; 
$BWA_MEM $BWAopt $REF_IDX $FQTMP 2>/dev/null | 
  $TAWK '/^@/||($2!=4)' |
    $SAMTOOLS_SORT $STSopt -o $BAM &>/dev/null ;

echo -n "." ;

## building consensus ##########################################################
# STCopt="--threads $NTHREADS --mode bayesian --ambig --min-MQ 2 --P-het 1.0e-10 --het-scale 1.0";
STCopt="--threads $NTHREADS --mode simple --min-MQ $MQ --min-BQ $BQ --min-depth $COV --use-qual --call-fract $FQ --het-fract $FH";
$AMB && STCopt="$STCopt --ambig";
SEQ=$($SAMTOOLS_CONSENSUS $STCopt $BAM 2>/dev/null |
	grep -v "^>" | tr -d '\n' |
          sed -r 's/^[ACGT]{0,5}[acgtRrYySsWwKkMmBbDdHhVvNn]+//;s/[acgtRrYySsWwKkMmBbDdHhVvNn]+[ACGT]{0,5}$//;' |  #  trimming low
          sed -r 's/^[ACGT]{0,5}[acgtRrYySsWwKkMmBbDdHhVvNn]+//;s/[acgtRrYySsWwKkMmBbDdHhVvNn]+[ACGT]{0,5}$//;' |  #  quality ends
          sed -r 's/^[ACGT]{0,5}[acgtRrYySsWwKkMmBbDdHhVvNn]+//;s/[acgtRrYySsWwKkMmBbDdHhVvNn]+[ACGT]{0,5}$//;' |  #  of  the  SSU
          sed -r 's/^[ACGT]{0,5}[acgtRrYySsWwKkMmBbDdHhVvNn]+//;s/[acgtRrYySsWwKkMmBbDdHhVvNn]+[ACGT]{0,5}$//;' |  #  sequence (if
          sed -r 's/^[ACGT]{0,5}[acgtRrYySsWwKkMmBbDdHhVvNn]+//;s/[acgtRrYySsWwKkMmBbDdHhVvNn]+[ACGT]{0,5}$//;');  #  any)

echo " [ok]" ;


##############################################################################################################
#                                                                                                            #
# FINALIZING                                                                                                 #
#                                                                                                            #
##############################################################################################################
l=${#SEQ};
r=$(tr -cd 'ACGT' <<<"$SEQ" | wc -m);
a=$(( $l - $r ))

if $VERBOSE
then
  echo "> $NR selected reads ($NB bases; lgt > $(( $RLGT - 1 )))" ;
  echo "> coverage depth: $DEPTH""""x" ;
  if   [ $a -eq 0 ]; then echo "> $l bps" ;
  elif [ $a -eq 1 ]; then echo "> $l bps (ambiguous base: $a)" ;
  else                    echo "> $l bps (ambiguous bases: $a)" ;
  fi
fi

if $VERBOSE
then
  fold -w 100 <<<"$SEQ" |
    $BAWK -v h1="$H1" -v h2="$H2" 'BEGIN{print"              "h1; print"              "h2;}
                                        {printf("%5s %s\n", 1+100*(NR-1), $1); printf"      "; w=split($1,c,""); i=0;
                                         while(++i<=w) switch(c[i]){ case "A": case "C": case "G": case "T": printf" "; continue; 
                                                                     default:                                printf"*"; continue; }
                                         print""; }' ;
fi

if [ $l -lt $SLGT ]
then
  echo "> sequence too short: $l bps" ;
  finalize ;
  echo "$(chrono) exit" ;
  exit ;
fi

pa=$(( 100 * $a / $l ));
if [ $pa -gt 5 ]
then
  echo "> too many ambiguous bases: $pa%" ;
  finalize ;
  echo "$(chrono) exit" ;
  exit ;
fi

echo ">contigSSU_covr_$DEPTH""_lgt_$l""_amb_$a"  > $OUTFILE ;
echo "$SEQ"                               >> $OUTFILE ;
if [ "$OUTFQ" == "$NA" ]
then
  echo "$(chrono) writing output file ... [ok]" ;
  $VERBOSE && echo "+ FASTA: $OUTFILE" ;
else
  mv $FQTMP $OUTFQ ;
  echo "$(chrono) writing output files ... [ok]" ;
  $VERBOSE && echo "+ FASTA: $OUTFILE" ;
  $VERBOSE && echo "+ FASTQ: $OUTFQ" ;
fi

finalize ;

echo "$(chrono) exit" ;

exit ;


